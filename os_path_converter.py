import os, re

# L:\                       X:\
# P:\project1\textures      Z:\library\textures

# Input paths:
# L:\temp
# P:/project1/textures\grass.tga
# P:\project1\assets\env\Forest
# cache\Tree.abc

# Expected result:
# X:\temp
# Z:\library\textures\grass.tga
# P:\project1\assets\env\Forest
# cache\Tree.abc

mappings = {"L:\\": "X:\\", "P:/project1/textures": "Z:\library\\textures"}
input_paths = ["L:\\temp", "P:/project1/textures\grass.tga", "P:\project1\\assets\env\Forest", "cache\\Tree.abc"]

mappings_two = {"L:\\": "/Volumes/l/", "P:\\" : "/Volumes/p/"}
input_paths_two = ["L:\temp", "P:/project1/textures\grass.tga", "P:\project1\assets\env\Forest", "cache\Tree.abc"]

mappings_three = {"L:\\": "/mnt/l/", "P:\\": "/mnt/p/", "/Volumes/l": "/mnt/l", "/Volumes/p": "/mnt/p"}
input_paths_three = ["L:\\temp", "P:/project1/textures\grass.tga", "P:\project1\\assets\env\Forest", "cache\Tree.abc", "/mnt/p/project1/assets/prop/Box", "/mnt/p/project1/textures/wood.tga", "/Volumes/l/project2/input/20190117", "/Volumes/l/project2/shots"]

def pathConverter(mappings, inputpaths, os="windows"):
    """ 
    function takes mappings dictionary and input paths list as parameters
    it first sets delimeter for selected os, serializes the data to be consistent,
    creates a dictionary containing old paths as key and new path as value
    last step is swapping initial paths list with newly created ones if it changed and returning final list
    """
    if os == "windows":
        delimeter = "\\"
    else:
        delimeter = "/"
    
    serialized_input_paths = [delimeter.join(re.split(r'[\\/]', i)) for i in inputpaths]
    serialized_mappings = {delimeter.join(re.split(r'[\\/]', k)) : delimeter.join(re.split(r'[\\/]', v)) for k, v in mappings.items() }
    remapped = {}
    for path in serialized_input_paths:
        for key in serialized_mappings:
            if path.startswith(key):
                new_path = path.replace(key, serialized_mappings[key])
                remapped.update({path:new_path})

    for i, path in enumerate(serialized_input_paths):
        if path in remapped:
            serialized_input_paths[i] = remapped[path]

    return serialized_input_paths

pathConverter(mappings_three, input_paths_three, os="mac")