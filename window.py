import sys
import re
from PyQt5.QtCore import QCoreApplication, Qt
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton, QAction, QMessageBox
from PyQt5.QtWidgets import QTextEdit, QFileDialog
from PyQt5.QtWidgets import QCheckBox, QLabel, QLineEdit, QInputDialog
from mappings import mappings


class window(QMainWindow):
    '''
    editing files is possible straight from the app itself
    mappings.py has to contain valid dictionary object
    paths.csv has to contain paths, each path in new line
    results are written to result.csv file
    '''

    def __init__(self):
        super(window, self).__init__()
        self.setGeometry(50, 50, 500, 300)
        self.setWindowTitle('path converter app')
        self.delimeter = "\\"
        self.mappings = mappings

        openFile = QAction('&Open File', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Open File')
        openFile.triggered.connect(self.file_open)

        saveFile = QAction('&Save File', self)
        saveFile.setShortcut('Ctrl+S')
        saveFile.setStatusTip('Save File')
        saveFile.triggered.connect(self.file_save)

        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('&File')
        fileMenu.addAction(openFile)
        fileMenu.addAction(saveFile)

        self.home()

    def editor(self):
        self.textEdit = QTextEdit()
        self.setCentralWidget(self.textEdit)

    def file_open(self):
        name, _ = QFileDialog.getOpenFileName(self, 'Open File', options=QFileDialog.DontUseNativeDialog)
        if not name:
            return

        file = open(name, 'r')
        self.editor()

        with file:
            text = file.read()
            self.textEdit.setText(text)

    def file_save(self):
        name, _ = QFileDialog.getSaveFileName(self,'Save File', options=QFileDialog.DontUseNativeDialog)
        if not name:
            return

        file = open(name, 'w')
        text = self.textEdit.toPlainText()
        file.write(text)
        file.close()

    def setOS(self, state):
        if state == Qt.Checked:
            self.delimeter = "\\"
        else:
            self.delimeter = "/"

    def pathConverter(self):
        """ 
        it first reads the data from files and serializes it be consistent,
        creates a dictionary containing old paths as key and new path as value
        last step is swapping initial paths list with newly created ones if it changed
        and writing results to a file
        """
        delimeter = self.delimeter
        with open('paths.csv') as f:
            read_data = f.readlines()
        inputpaths = [i for i in read_data]
        
        serialized_input_paths = [delimeter.join(re.split(r'[\\/]', i)) for i in inputpaths]
        serialized_mappings = {delimeter.join(re.split(r'[\\/]', k)) : delimeter.join(re.split(r'[\\/]', v)) for k, v in self.mappings.items() }
        remapped = {}
        for path in serialized_input_paths:
            for key in serialized_mappings:
                if path.startswith(key):
                    new_path = path.replace(key, serialized_mappings[key])
                    remapped.update({path:new_path})

        for i, path in enumerate(serialized_input_paths):
            if path in remapped:
                serialized_input_paths[i] = remapped[path]
        
        with open('result.csv', 'w') as result_file:
            for path in serialized_input_paths:
                result_file.write(path)
        # return serialized_input_paths

    def home(self):
        btn = QPushButton('quit', self)
        btn.clicked.connect(self.close_application)
        btn.resize(btn.sizeHint())
        btn.move(10, 270)

        checkBox = QCheckBox('check for backslash, uncheck for slash path delimeter', self)
        checkBox.toggle()
        checkBox.move(10, 25)
        checkBox.resize(checkBox.sizeHint())
        checkBox.stateChanged.connect(self.setOS)

        remap_btn = QPushButton('remap the paths', self)
        remap_btn.clicked.connect(self.pathConverter)
        remap_btn.resize(100, 40)
        remap_btn.move(10, 100)
        
        self.show()

    def close_application(self):

        choice = QMessageBox.question(self, 'Message',
                                     "Are you sure to quit?", QMessageBox.Yes |
                                     QMessageBox.No, QMessageBox.No)

        if choice == QMessageBox.Yes:
            print('quit application')
            sys.exit()
        else:
            pass


if __name__ == "__main__":

    def run():
        app = QApplication(sys.argv)
        Gui = window()
        sys.exit(app.exec_())

run()